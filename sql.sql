CREATE TABLE `customers`
(
    `id`            INT(11) NOT NULL AUTO_INCREMENT,
    `customer_name` VARCHAR(255) DEFAULT NULL,
    `gender`        INT(11) NOT NULL COMMENT '0 – пол не указан, 1 - юноша, 2 - девушка.',
    `birth_date`    INT(11) NOT NULL COMMENT 'День рождения в Unix Time Stamp.',
    PRIMARY KEY (`id`)
);

CREATE TABLE `books`
(
    `id`          INT(11) NOT NULL AUTO_INCREMENT,
    `customer_id` INT(11) NOT NULL,
    `author_id`   INT(10) NOT NULL,
    `book_title`  VARCHAR(255) DEFAULT NULL
);

CREATE TABLE `authors`
(
    `id`          INT(11) NOT NULL AUTO_INCREMENT,
    `author_name` VARCHAR(25) DEFAULT NULL
);