<p align="center"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Hi, its me...

Step 1. clone repository:
```ssh
git@gitlab.com:tests302/malltech.git
```

Step 2. copy .env.example to .env:
```ssh
cp .env.example .env
```

Step 3. Set up a database connection in .env.

Step 4. Run composer update:

```ssh
composer update
```

Step 5. Migrate and seed the database:

```ssh
php artisan migrate:seed
```

Step 6. Generate app key:

```ssh
php artisan key:generate
```

Step 7. Run app:
```ssh
php artisan serve
```

Go to:
[http://127.0.0.1:8000/customers](http://127.0.0.1:8000/customers)

You can use filters:
[http://127.0.0.1:8000/customers?user_ids=1,2,3,4,5,6,7&years_from=15&years_till=20](http://127.0.0.1:8000/customers?user_ids=1,2,3,4,5,6,7&years_from=15&years_till=20)

P.S.: Displays a list of boys and girls only. gender=none is not in the selection.
