<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;

class CustomerController extends Controller
{
  public function getUsers(Request $request): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
  {
    //Не вижу смысла получать имя автора. Фронт отдаст id, если у него есть список
    if (filled($request->get('author_ids'))) {
      $with = [
        'books.authors' => function($q) use ($request) {
          $q->whereIn('id', $request->get('author_ids'));
        }
      ];
    } else {
      $with = [
        'books.authors'
      ];
    }

    $query = User::with($with)->whereIn('gender', ['male', 'female']);

    if (filled($request->get('user_ids'))) {
      $query->whereIn('id', explode(',', $request->get('user_ids')));
    }

    if (filled($request->get('years_from'))) {
      $query->where('birth_date', '<=', now()->subYears((int)$request->get('years_till')));
    }

    if (filled($request->get('years_till'))) {
      $query->where('birth_date', '>=', now()->subYears((int)$request->get('years_to')));
    }

    $users = $query->get();

    return UserResource::collection($users);
  }
}
