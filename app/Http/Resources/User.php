<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param Request $request
   * @return array
   *
   */
  public function toArray($request): array
  {
    $books = $this->whenLoaded('books', function () {
      return Book::collection($this->books);
    }, null);

    return [
      'name' => $this->name,
      'gender' => $this->gender,
      'birth_date' => $this->birth_date->format('d.m.Y'),
      'email' => $this->birth_date->format('d.m.Y'),
      'books' => $books ?? [],
    ];

  }
}
