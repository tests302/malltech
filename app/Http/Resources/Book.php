<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class Book extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param Request $request
   * @return array
   */
  public function toArray($request): array
  {
    $authors = $this->whenLoaded('authors', function () {
      return Author::collection($this->authors);
    }, null);

    return [
      'book_title' => $this->book_title,
      'authors' => $authors ?? [],
    ];
  }
}
