<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    for ($i = 0; $i < (int)env('SEED_USERS_COUNT', 15); $i++) {
      $user = User::create([
          'name' => "test_{$i}",
          'gender' => $this->getRandomGender(),
          'birth_date' => $this->getRandomDate(now()->subYears(28), now()->subYears(10)),
          'email' => $this->getRandomString(),
          'password' => Hash::make($this->getRandomString()),
        ]);

      $user->books()->sync($this->getRandomBookIds());
    }
  }

  private function getRandomString($length = 10): string
  {
    $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';

    return substr(str_shuffle($permitted_chars), 0, $length);
  }

  private function getRandomGender(): string
  {
    $genders = [
      'none',
      'male',
      'female',
    ];

    return $genders[rand(0, 2)];
  }

  private function getRandomDate(Carbon $start, Carbon $end): Carbon
  {
    $randomTimestamp = mt_rand($start->getTimestamp(), $end->getTimestamp());
    $randomDate = new Carbon();
    $randomDate->setTimestamp($randomTimestamp);

    return $randomDate;
  }

  private function getRandomBookIds(int $maxCountIds = 5)
  {
    $countIds = rand(1, $maxCountIds);

    return Book::orderByRaw('rand()')->take($countIds)->pluck('id');
  }
}
