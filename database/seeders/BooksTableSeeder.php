<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    for ($i = 0; $i < (int)env('SEED_BOOKS_COUNT', 20); $i++) {
      $book = Book::create([
          'book_title' => "test_{$i}",
        ]
      );

      $book->authors()->sync($this->getRandomAuthorIds());
    }
  }

  private function getRandomAuthorIds(int $maxCountIds = 2)
  {
    $countIds = rand(1, $maxCountIds);

    return Author::orderByRaw('rand()')->take($countIds)->pluck('id');
  }
}
