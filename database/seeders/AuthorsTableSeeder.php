<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AuthorsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $authors = [
      'Федор Достоевский',
      'Михаил Булгаков',
      'Александр Пушкин',
      'Николай Гоголь',
      'Лев Толстой',
      'Антон Чехов',
      'Александр Дюма',
      'Иван Тургенев',
      'Эрих Мария Ремарк',
      'Артур Конан Дойль',
      'Виктор Гюго',
      'Жюль Верн',
      'Аркадий и Борис Стругацкие',
      'Джек Лондон',
      'Эрнест Хемингуэй',
      'Михаил Шолохов',
    ];

    foreach ($authors as $author) {
      Author::create([
          'author_name' => $author,
        ]
      );
    }
  }
}
